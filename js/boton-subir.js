
// condiciona el boton subir

		jQuery(document).ready(function() {
			// Show or hide the sticky footer button
			jQuery(window).scroll(function() {
				if (jQuery(this).scrollTop() > 200) {
					jQuery('#boton-subir').fadeIn(200);
				} else {
					jQuery('#boton-subir').fadeOut(200);
				}
			});

			// Animate the scroll to top
			jQuery('#boton-subir').click(function(event) {
				event.preventDefault();

				jQuery('html, body').animate({scrollTop: 0}, 300);
			})
		});