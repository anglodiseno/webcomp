<?php
/*
Plugin Name: WebComp
Plugin URI: http://www.anglo.cl/webcomp
Description: Añade elementos al tema: Boton Subir
Version: 0.1.1
Author: André Ribet
Author URI: http://www.anglo.cl/webcomp
License: GPL2
GitHub Plugin URI: https://gitlab.com/anglodiseno/webcomp
*/

/*  Copyright 2016 André Ribet  (email : andre@anglo.cl)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Inserta codigo js y css, para boton subir
	add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
	function theme_name_scripts() {
		
	    // nos aseguramos que no estamos en el area de administracion
	    if( !is_admin() ){
			
			// registramos nuestro script con el nombre "mi-script" y decimos que es dependiente de jQuery para que wordpress se asegure de incluir jQuery antes de este archivo
			// en adicion a las dependencias podemos indicar que este archivo debe ser insertado en el footer del sitio, en el lugar donde se encuentre la funcion wp_footer
			wp_register_script('script-subir', plugins_url('webcomp/js/boton-subir.js'), array('jquery'), '1', true );
			wp_enqueue_script('script-subir');

			// añade style.css
			wp_enqueue_style('style-subir', plugins_url('/webcomp/css/style.css') );
		}
	}

// Html del elemento boton-subir insertado en el footer del sitio, en el lugar donde se encuentre la funcion wp_footer
// Se considera el uso de ED-Grid, framework css v1.2, para el icono de flecha "ariba"	
	add_action('wp_footer', 'subir');
	function subir() {

		$textoBoton	=	"subir"; // texto en el boton
		$idBoton	=	"boton-subir"; //
		$classBoton	=	"icon-arriba otro";
		$hrefBoton	=	"#";

		echo '<a id="'.$idBoton.'" class="'.$classBoton.'" href="'.$hrefBoton.'">'.$textoBoton.'</a>';
	}

?>
